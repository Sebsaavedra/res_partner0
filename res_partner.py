# -*- coding: utf-8 -*-
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2009-TODAY Odoo Peru(<http://www.odooperu.pe>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import logging
from itertools import cycle
import sys

from odoo import models, fields, api
from odoo.exceptions import Warning, ValidationError



class ResPartner(models.Model):
    _inherit = 'res.partner'
    _vat = 'vat'


    vat = fields.Char(string='RUT', required=True, translate=True, store=True, compute_sudo=False)


    registration_name = fields.Char('Name', size=128, index=True)
    catalog_06_id = fields.Many2one('einvoice.catalog.06', 'Tipo Doc.', index=True)
    state = fields.Selection([('habido', 'Habido'), ('nhabido', 'No Habido')], 'State')


    @api.onchange('vat')
    def vat_change(self):
      #raise Warning(self.vat)
      #self.validarRut(self.vat)
      self.update_document()


    @api.one
    def update_document(self):
        if not self.vat:
            return False
        if self.vat and len(self.vat) != 10:
            raise Warning('EL RUT DEBE TENER 10 CARACTERES')
        else:
            result = self.validarRut(self.vat)
            if (result == False):
                raise Warning('EL RUT INGRESADO ES INVALIDO')
            else:
                #raise Warning('RUT INGRESADO: CORRECTO')
                self.check_vatnumber()
                return True


    @api.constrains('vat')
    def check_vatnumber(self):
        for record in self:
            obj = self.search([('vat', '=', record.vat)])
            if len(obj) > 0:
               raise Warning('ATENCIÓN, EL RUT INGRESADO, YA EXISTE. INTENTE NUEVAMENTE.')
            else:
                return True

    def validarRut(self, rut):
        rut = rut.upper();
        rut = rut.replace("-", "")
        rut = rut.replace(".", "")
        aux = rut[:-1]
        dv = rut[-1:]
        revertido = map(int, reversed(str(aux)))
        factors = cycle(range(2, 8))
        s = sum(d * f for d, f in zip(revertido, factors))
        res = (-s) % 11
        if str(res) == dv:
            return True
        elif dv == "K" and res == 10:
            return True
        else:
            return False



